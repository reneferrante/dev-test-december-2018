<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizanod a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a funação playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
		
	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}
	
	// compares two players and returns values for ordenation
	private function cmp($a, $b)
	{
		// players have same score: proceeds to compare number of games
		if($a['score'] == $b['score'])
		{
			// players have same number of games: do not alter currently order 
			if($a['games_played'] == $b['games_played'])
			{
				return $a['index']-$b['index'];
			}
			// player a has less games: do not alter order
			// else: alter order
			return ($a['games_played'] < $b['games_played']) ? -1 : 1;
		}
		// player a has major score: do not alter order
		// else: alter order
		return ($a['score'] > $b['score']) ? -1 : 1;
	}


	// ordenate standings to match current rank
	private function rankPlayers()
	{
		// copy of standings
		$ranking = $this->standings;

		// ordenate ranking
		uasort($ranking, array($this, 'cmp'));

		// return ranking
		return $ranking;
	}

	// return the player ranked at position passed
	public function playerRank($rank)
	{
		// get current ranking
		$ranking = $this->rankPlayers();

		// get just the players of the ranking created 
		$playersRank = array_keys($ranking);

		// return the player at position passed 
		// (array starts from 0 but positions in rank from 1)
		return $playersRank[$rank-1];
	}
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);