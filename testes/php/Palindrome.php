<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    // check if word is palindrome
    public static function isPalindrome($word)
    {
        // get new string of just lowercase characters
        $lowercase = strtolower($word);

        // reverted string is the same as string: return true
        // else: return false
        return (strrev($lowercase) == $lowercase) ? true : false;
    }
}

echo Palindrome::isPalindrome('Deleveled');