<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    // group files by owners
    public static function groupByOwners($files)
    {
    	// initialize response array
        $response = array();

        // populate response with owners and respective files
        foreach ($files as $file => $owner) {
            // owner not in response list: initialize owner file list
            if(!array_key_exists($owner, $response)){
                $response[$owner] = array();
            }

            // add file to owner file list
            $response[$owner][] = $file;
        }

        // return response
        return $response;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));