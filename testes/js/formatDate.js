// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

// format from MM/DD/YYYY to YYYYMMDD
function formatDate(userDate) {
  // get date components
  var date = userDate.split("/");
  
  // return date in API format
  return date[2]+date[0]+date[1];
}

console.log(formatDate("12/31/2014"));