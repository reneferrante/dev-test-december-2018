// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

// remove property of object
function removeProperty(obj, prop) {
  // object does not have property: return false
  if(!obj.hasOwnProperty(prop)){
    return false;
  }

  // remove object property
  delete obj[prop];
  return true;
}